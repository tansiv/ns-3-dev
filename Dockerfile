FROM debian:stable


RUN apt-get update

RUN apt-get install -y git g++ python3 cmake ninja-build ccache tcpdump

WORKDIR /ns3
COPY . /ns3/

RUN ./ns3 clean
RUN ./ns3 configure --disable-examples --disable-modules wifi --build-profile=optimized
RUN ./ns3 build

# Add ns-3 to ld
RUN echo "/ns3/build/lib" > /etc/ld.so.conf.d/ns3.conf
RUN ldconfig